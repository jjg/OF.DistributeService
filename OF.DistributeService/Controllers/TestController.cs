﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com).

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


using OF.DistributeService.Core.Attribute;
using OF.DistributeService.Core.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace OF.DistributeService.Controllers
{
    public class TestController : ApiController
    {
        [HttpPost]
        public int TestAdd(QuerySORequest req)
        {
            return req.Id + 1;
        }


        [HttpPut]
        [OFDistributeServiceMethod(HttpMethod=ApiHttpMethod.Put)]
        public int TestPut(QuerySORequest req)
        {
            return req.Id + 1;
        }

        [HttpDelete]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Delete)]
        public int DeleteSO(int id, string name)
        {
            return 1;
        }

        [HttpGet]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Get)]
        public Task<int> GetSO(int id)
        {
            /*
            while (true)
            {
                Thread.Sleep(1000);
            }*/
            return Task.Factory.StartNew<int>(() => {
                return id + 1;
            });
        }
        [HttpPost]
        public string GetCookie(QuerySORequest req)
        {
            int oldCookieValue = 0;
            if (HttpContext.Current.Request.Cookies["TestCookie"] != null)
            {
                oldCookieValue = int.Parse(HttpContext.Current.Request.Cookies["TestCookie"].Value);
            }
            int result = (req.Id + 1) + oldCookieValue;
            HttpCookie cook = new HttpCookie("TestCookie");
            cook.Value = result.ToString();
            HttpContext.Current.Response.Cookies.Add(cook);
            return result.ToString();
        }
    }
}
