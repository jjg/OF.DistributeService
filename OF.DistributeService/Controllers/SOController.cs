﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com).

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


using OF.DistributeService.Controllers;
using OF.DistributeService.Core.Attribute;
using OF.DistributeService.Core.Entity.Service;
using OF.DistributeService.SO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace OF.DistributeService.SO
{
    [OFDistributeService(ServiceName = "SO", Version = "1", DefaultMethod = ApiHttpMethod.Get, VirtualPath = "/SO")]
    public interface ISOService
    {
        Task<CallServiceResult<List<QuerySODTO>>> QuerySO3(int id);
        Task<CallServiceResult<List<QuerySODTO>>> QueryJSONSO3(int id);

        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post, ContentFormat = ApiContentFormat.JSON)]
        Task<CallServiceResult<QuerySODTO>> PostJSONSO3(QuerySORequest request);

        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post, ContentFormat = ApiContentFormat.MSGPack)]
        Task<CallServiceResult<QuerySODTO>> PostSO3(QuerySORequest request);

        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        CallServiceResult<QuerySODTO> AddJSONSO(QuerySORequest request);
        CallServiceResult<List<QuerySODTO>> QueryJSONSO(DateTime? dt, int? id, int? c);

        CallServiceResult<List<QuerySODTO>> QuerySO(DateTime? dt, int? id, int? c);
        CallServiceResult<List<QuerySODTO>> QuerySO2(int id, string name);

        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        CallServiceResult<QuerySODTO> AddSO(QuerySORequest request);

        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Delete)]
        int DeleteSO(int id, string name);
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Put)]
        QuerySORequest UpdateSO(QuerySORequest request);
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Delete)]
        void DeleteSO2(int id);
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Put)]
        void UpdateSO2(QuerySORequest request);

        int GetNParams(int? a, int? b, int? c, int? d, int? e, int? f, int? g, string h, string i);

        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        TestEntity TestPost(TestEntity entity);
        TestEntity TestGet(System.Boolean a, System.Byte b, System.Char c, System.DateTime d, System.Decimal e, System.Double f, System.Guid g, System.Int32 h, System.Nullable<int> i, System.String j, System.UInt32 k);
    }
}

namespace OF.DistributeService.Controllers
{
    
    public class SOController : ApiController, ISOService
    {
        public string Test()
        {
            return "1";
        }

        [AcceptVerbs("GET")]
        public TestEntity TestGet(System.Boolean a, System.Byte b, System.Char c, System.DateTime d, System.Decimal e, System.Double f, System.Guid g, System.Int32 h, System.Nullable<int> i, System.String j, System.UInt32 k) 
        {
            return new TestEntity { 
                a = a,
                b= b,c = c, d = d, e = e, f= f, g = g,h = h, i = i, j = j, k = k
            };
        }
        
        [AcceptVerbs("POST")]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        public TestEntity TestPost(TestEntity entity)
        {
            return entity;
        }

        public string GetServiceProvider()
        {
            return HttpContext.Current.Request.Url.AbsoluteUri;
        }

        [AcceptVerbs("GET")]
        public CallServiceResult<List<QuerySODTO>> QuerySO(DateTime? dt, int? id, int? c)
        {
            return new CallServiceResult<List<QuerySODTO>>
            {
                IsSuccess = true,
                Result = new List<QuerySODTO>{
                    new QuerySODTO{
                         Id = (id ?? 0) + 2 + (c?? 10),
                         Amount = (id ?? 0) + 1+ (c?? 10),
                         DateTime = dt
                    }
                },
                ServiceHost = GetServiceProvider()
            };
        }

        [AcceptVerbs("GET")]
        public CallServiceResult<List<QuerySODTO>> QueryJSONSO(DateTime? dt, int? id, int? c)
        {
            return new CallServiceResult<List<QuerySODTO>>
            {
                IsSuccess = true,
                Result = new List<QuerySODTO>{
                    new QuerySODTO{
                         Id = (id ?? 0) + 2 + (c?? 10),
                         Amount = (id ?? 0) + 1+ (c?? 10),
                         DateTime = dt
                    }
                },
                ServiceHost = GetServiceProvider()
            };
        }

        [AcceptVerbs("GET")]
        public Task<CallServiceResult<List<QuerySODTO>>> QuerySO3(int id)
        {
            string serviceProvider = GetServiceProvider();
            TaskCompletionSource<CallServiceResult<List<QuerySODTO>>> ts = new TaskCompletionSource<CallServiceResult<List<QuerySODTO>>>();
            Task.Factory.StartNew(() => {
                Task.Delay(1000);
                ts.SetResult(new CallServiceResult<List<QuerySODTO>>
                {
                    IsSuccess = true,
                    Result = new List<QuerySODTO>{
                    new QuerySODTO{
                         Id = id + 4,
                         Amount = id + 3,
                         DateTime = DateTime.Parse("2016-01-01 00:00:00.000")
                    }
                },
                    ServiceHost = serviceProvider
                });
            });
            return ts.Task;
        }

        [AcceptVerbs("POST")]
        public Task<CallServiceResult<QuerySODTO>> PostSO3(QuerySORequest request)
        {
            string serviceProvider = GetServiceProvider();
            TaskCompletionSource<CallServiceResult<QuerySODTO>> ts = new TaskCompletionSource<CallServiceResult<QuerySODTO>>();
            Task.Factory.StartNew(() =>
            {
                Task.Delay(1000);
                ts.SetResult(new CallServiceResult<QuerySODTO>
                {
                    IsSuccess = true,
                    Result = new QuerySODTO{
                        Id = request.Id + 2,
                        Amount = request.Id + 1,
                        DateTime = request.Dt
                    },
                    ServiceHost = serviceProvider
                });
            });
            return ts.Task;
        }

        [AcceptVerbs("POST")]
        public Task<CallServiceResult<QuerySODTO>> PostJSONSO3(QuerySORequest request)
        {
            string serviceProvider = GetServiceProvider();
            TaskCompletionSource<CallServiceResult<QuerySODTO>> ts = new TaskCompletionSource<CallServiceResult<QuerySODTO>>();
            Task.Factory.StartNew(() =>
            {
                Task.Delay(1000);
                ts.SetResult(new CallServiceResult<QuerySODTO>
                {
                    IsSuccess = true,
                    Result = new QuerySODTO
                    {
                        Id = request.Id + 2,
                        Amount = request.Id + 1,
                        DateTime = request.Dt
                    },
                    ServiceHost = serviceProvider
                });
            });
            return ts.Task;
        }

        [AcceptVerbs("GET")]
        public Task<CallServiceResult<List<QuerySODTO>>> QueryJSONSO3(int id)
        {
            string serviceProvider = GetServiceProvider();
            TaskCompletionSource<CallServiceResult<List<QuerySODTO>>> ts = new TaskCompletionSource<CallServiceResult<List<QuerySODTO>>>();
            Task.Factory.StartNew(() =>
            {
                Task.Delay(1000);
                ts.SetResult(new CallServiceResult<List<QuerySODTO>>
                {
                    IsSuccess = true,
                    Result = new List<QuerySODTO>{
                    new QuerySODTO{
                         Id = id + 4,
                         Amount = id + 3,
                         DateTime = DateTime.Parse("2016-01-01 00:00:00.000")
                    }
                },
                    ServiceHost = serviceProvider
                });
            });
            return ts.Task;
        }

        [HttpPost]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        public CallServiceResult<QuerySODTO> AddSO(QuerySORequest request)
        {
            return new CallServiceResult<QuerySODTO>
            {
                IsSuccess = true,
                Result = new QuerySODTO
                {
                    Id = request.Id + 2,
                    Amount = request.Id + 1,
                    DateTime = request.Dt
                },
                ServiceHost = GetServiceProvider()
            };
        }


        [HttpPost]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        public CallServiceResult<QuerySODTO> AddJSONSO(QuerySORequest request)
        {
            return new CallServiceResult<QuerySODTO>
            {
                IsSuccess = true,
                Result = new QuerySODTO
                {
                    Id = request.Id + 2,
                    Amount = request.Id + 1,
                    DateTime = request.Dt
                },
                ServiceHost = GetServiceProvider()
            };
        }

        [HttpDelete]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Delete)]
        public int DeleteSO(int id, string name)
        {
            return 1 + name.Length;
        }

        [HttpPut]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Put)]
        public QuerySORequest UpdateSO(QuerySORequest request)
        {
            request.Id = request.Id + 1;
            return request;
        }


        [AcceptVerbs("GET")]
        public CallServiceResult<List<QuerySODTO>> QuerySO2(int id, string name)
        {
            return new CallServiceResult<List<QuerySODTO>>
            {
                IsSuccess = true,
                Result = new List<QuerySODTO>{
                    new QuerySODTO{
                         Id = id + 2 + name.Length,
                         Amount = id + 1 + name.Length
                    }
                },
                ServiceHost = GetServiceProvider()
            };
        }

        [AcceptVerbs("DELETE")]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Delete)]
        public void DeleteSO2(int id)
        {

        }

        [AcceptVerbs("PUT")]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Put)]
        public void UpdateSO2(QuerySORequest request)
        {

        }

        [AcceptVerbs("POST")]
        [OFDistributeServiceMethod(HttpMethod = ApiHttpMethod.Post)]
        public CallServiceResult<int> AddSO2(QuerySORequest request)
        {
            return new CallServiceResult<int>
            {
                IsSuccess = true,
                Result = request.Id,
                ServiceHost = GetServiceProvider()
            };
        }

        public int GetNParams(int? a, int? b, int? c, int? d, int? e, int? f, int? g, string h, string i)
        {
            return a?? 0 + b?? 0 + c?? 0 + d?? 0 + e?? 0 + f?? 0 + g?? 0 + h.Length + i.Length;
        }
    }


    

    public class TestEntity
    {
        public System.Boolean a;
        public System.Byte b;
        public System.Char c;
        public System.DateTime d;
        public System.Decimal e;
        public System.Double f;
        public System.Guid g;
        public System.Int32 h;
        public System.Nullable<int> i;
        public System.String j;
        public System.UInt32 k;
        public int[] l;
        public List<int> m;
        public byte[] n;
        public TestEntity o;
        public TestEntity[] p;
        public List<TestEntity> q;
    }

    public class QuerySORequest
    {
        public int Id
        { get; set; }

        public DateTime? Dt
        { get; set; }
    }

    public class QuerySODTO
    {
        public int Id
        { get; set; }

        public decimal Amount
        { get; set; }

        public DateTime? DateTime
        { get; set; }
    }
}