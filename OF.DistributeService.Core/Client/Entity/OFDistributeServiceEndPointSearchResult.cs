﻿using OF.DistributeService.Core.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Client.Entity
{
    public class OFDistributeServiceEndPointSearchResult
    {
        public string ServiceUrl;
        public MethodWithHttpMethod MethodInfo;
    }
}
