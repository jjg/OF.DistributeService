﻿using OF.DistributeService.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Client.Entity
{

    public class InstanceListModify
    {
        public List<string> DeleteList;
        public List<string> InsertList;
        public List<string> UpdateList;

        private static StringEqualityComparer StringComparer = new StringEqualityComparer();
        public InstanceListModify(List<string> oldList, List<string> newList)
        {
            if (oldList == null || oldList.Count == 0)
            {
                this.DeleteList = null;
                this.UpdateList = null;
                this.InsertList = newList;
            }
            else if (newList == null || newList.Count == 0)
            {
                this.DeleteList = oldList;
                this.UpdateList = null;
                this.InsertList = null;
            }
            else
            {
                this.UpdateList = (from oldItem in oldList
                                   from newItem in newList
                                   where oldItem.Equals(newItem, StringComparison.InvariantCultureIgnoreCase)
                                   select oldItem).ToList();
                this.DeleteList = oldList.Except(this.UpdateList, StringComparer).ToList();
                this.InsertList = newList.Except(this.UpdateList, StringComparer).ToList();
            }
        }
    }

}
