﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com).

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Entity
{
    public class AppConfig : ConfigurationSection
    {
        /// <summary>
        /// api 站点的根路径，完整格式如：http://www.site.com/{ApplicationRootPath}
        /// </summary>
        [ConfigurationProperty("APIBaseUrl", IsRequired = true)]
        public string APIBaseUrl
        {
            get { return (string)this["APIBaseUrl"]; }
            set { this["APIBaseUrl"] = value; }
        }

        /// <summary>
        /// ; 分隔
        /// </summary>
        [ConfigurationProperty("ServiceAssemblys", IsRequired = true)]
        public string ServiceAssemblys
        {
            get { return (string)this["ServiceAssemblys"]; }
            set { this["ServiceAssemblys"] = value; }
        }

        [ConfigurationProperty("OFDistributeServiceZKRootPath", IsRequired = true)]
        public string OFDistributeServiceZKRootPath
        {
            get { return (string)this["OFDistributeServiceZKRootPath"]; }
            set { this["OFDistributeServiceZKRootPath"] = value; }
        }

        [ConfigurationProperty("ApplicationName", IsRequired = true)]
        public string ApplicationName
        {
            get { return (string)this["ApplicationName"]; }
            set { this["ApplicationName"] = value; }
        }

        [ConfigurationProperty("GroupName", IsRequired = true)]
        public string GroupName
        {
            get { return (string)this["GroupName"]; }
            set { this["GroupName"] = value; }
        }

        [ConfigurationProperty("ZookeeperHostPort", IsRequired = true)]
        public string ZookeeperHostPort
        {
            get { return (string)this["ZookeeperHostPort"]; }
            set { this["ZookeeperHostPort"] = value; }
        }

        [ConfigurationProperty("ZookeeperSessionTimeSpan", IsRequired = true)]
        public int ZookeeperSessionTimeSpan
        {
            get { return (int)this["ZookeeperSessionTimeSpan"]; }
            set { this["ZookeeperSessionTimeSpan"] = value; }
        }

        private static AppConfig config;
        public static AppConfig Get()
        {
            return config;
        }

        static AppConfig()
        {
            config = ConfigurationManager.GetSection("AppConfig") as AppConfig;
        }
    }
}
