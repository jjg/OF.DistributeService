﻿using OF.DistributeService.Core;
using OF.DistributeService.Core.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OF.DistributeService.Server.Service
{
    public class OFDistributeServiceLiveController : ApiController
    {
        public string Get()
        {
            return "OK";
        }
    }
}
